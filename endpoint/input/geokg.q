[QueryItem="a_within_b"]
prefix geokg: <http://data.ign.fr/id/geokg/>
prefix topo: <http://data.ign.fr/def/topo#>
prefix geo: <http://www.opengis.net/ont/geosparql#>
PREFIX geof:<http://www.opengis.net/def/function/geosparql/>

SELECT * WHERE {
?n1 a geokg:haras ; geo:asWKT ?w1 .
?n2 a geokg:citerne ; geo:asWKT ?w2 .
FILTER(geof:sfWithin(?w2, ?w1)).
}
[QueryItem="a_contains_b"]
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
prefix owl: <http://www.w3.org/2002/07/owl#>
prefix geokg: <http://data.ign.fr/id/geokg/>
prefix topo: <http://data.ign.fr/def/topo#>
prefix geo: <http://www.opengis.net/ont/geosparql#>
PREFIX : <http://www.ontotext.com/plugins/geosparql#>
PREFIX geof:<http://www.opengis.net/def/function/geosparql/>


SELECT ?n1 ?n2 ?w1 ?w2 ?g1 ?g2 ?t1 
WHERE {
geokg:source geo:asWKT ?w1.


    
    
    }
[QueryItem="crs"]
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX ignf: <http://data.ign.fr/def/ignf#>
SELECT ?b ?p ?o
WHERE {
?s rdfs:label ?l.
?s ignf:baseCRS ?b.
?b ?p ?o.
FILTER regex(?l, ".*Lambert II etendu").
}